source .env

echo "#####################################################"
echo "### Installer step 1/3: Get Drupal 9.3.x codebase ###"
echo "#####################################################"
sleep 2
./docker/bin/composer create-project --no-interaction drupal/recommended-project:^9.3 .
./docker/bin/composer require --no-interaction drush/drush drupal/console

printf "\n"
echo "######################################################"
echo "### Installer step 2/3: Install database           ###"
echo "######################################################"
sleep 2
./docker/bin/drush site:install --db-url=mysql://$DB_USER_NAME:$DB_USER_PSWD@mariadb:3306/$DB_NAME -y --site-name=$COMPOSE_PROJECT_NAME standard

printf "\n"
echo "######################################################"
echo "### Installer step 3/3: Configure Drupal           ###"
echo "######################################################"
sleep 2
sudo chmod 777 drupal-root/web/sites/default/files/ -R
./docker/bin/drush cache:rebuild
./docker/bin/drupal site:status

printf "\n"
echo "######################################################"
echo "### Visit your Drupal website at http://localhost  ###"
echo "######################################################"