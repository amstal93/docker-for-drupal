# Docker for Drupal

This Docker template is meant to run a Drupal application in Linux, considering the official [Drupal system requirements](https://www.drupal.org/docs/system-requirements). Also, it is shipped with a curated collection of developer tools.

**Drupal 9.3 current Docker container strategy:** 
* PHP 8.1 with Apache 2, powered by Debian 11 "Bullseye"
* MariaDB 10.7
* phpMyAdmin 5.1
* Redis 6.2

## Contents

1. [How to use this Docker template for Drupal](#how-to-use-this-drupal-docker-template)
   - [Requirements](#requirements)
   - [Installation](#installation)
   - [Customization and advanced configuration](#customization-and-advanced-configuration)
   - [Managing your Drupal application](#managing-your-drupal-application)
2. [License](#license)
3. [About](#about)


## How to use this Drupal Docker template

### Requirements:

- [Linux OS](https://en.wikipedia.org/wiki/List_of_Linux_distributions)
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Installation:
1. Clone this project from Gitlab: `git clone https://gitlab.com/deviktta/docker-for-drupal.git`

2. Edit the Docker `.env` file and customize it with your own values.

3. Run the Docker containers: `docker-compose up -d --build`

4. Install your Drupal application, either by:
   - Existing website: place the codebase under `/drupal-root/` directory 
   - New website: run `./docker/bin/install_new_drupal.sh`

5. Visit your site by navigating to http://localhost.


### Managing your Drupal application:

Application logs can be found in `./docker/logs` directory.

**Available tools (see executables in `./docker/bin/`)**
* Composer: `./docker/bin/composer`
* Drush: `./docker/bin/drush`
* Drupal Console: `./docker/bin/drupal`
* PHP Code Sniffer: `./docker/bin/phpcs`
* PhpStan: `./docker/bin/phpstan`
* MySQL: `./docker/bin/mysql` `./docker/bin/mysqldump`
* Redis: `./docker/bin/redis-cli`

**Common operations**

Documentation available: visit [Drupal common operations](docs/drupal-common-operations.md) to see a full list on how to interact with Drupal through this Docker template (database back-up and restore, drush commands, ...).


### Customization and advanced configuration:

**Drupal integrations**
* [How to use Redis as Drupal cache](docs/how-to-use-redis-as-drupal-cache.md)

**Git**
* Available hooks under [docs/git-hooks](docs/git-hooks) folder.

**PHP configuration files**
* php.ini: [docker/phpserver/php.ini](docker/phpserver/php.ini)
* opcache: [docker/phpserver/opcache.ini](docker/phpserver/opcache.ini)
* xdebug: [docker/phpserver/xdebug.ini](docker/phpserver/xdebug.ini)

**Apache**
* Virtual hosts: [docker/phpserver/sites-enabled/000-default.conf](docker/phpserver/sites-enabled/000-default.conf)


## License
This project is under [MIT License](https://gitlab.com/deviktta/docker-for-drupal/-/blob/master/LICENSE.txt).


## About
Contact e-mail: [deviktta@protonmail.com](mailto:deviktta@protonmail.com); made with love from Barcelona, Catalunya.
