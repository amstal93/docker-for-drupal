# Drupal common operations

## drush:
  - Clearing cache: `./bin/drush cr`
  - Update database: `./bin/drush updb`
  - Get admin autologin link: `./bin/drush uli --uid=1`
  - Export database: `./bin/drush sql-dump --result-file=database-backup.sql`
  - Import database: `./bin/drush sql-cli < drupal-root/web/database-backup.sql`

## mysql:
  - Import database: `./bin/mysql -u user -ppass margath < database-backup.sql`
  - Export database: `./bin/mysqldump -u user -ppass margath > database-backup.sql`

## PhpMyAdmin:
  - Navigate to http://localhost:8081 to access.